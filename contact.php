<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

// Vérification des contenus

if(!isset($_POST['name']) || !is_string($_POST['name']) || !strlen($_POST['name'])) {   
    echo json_encode(array("code" => "KO","msg" => 'Veuillez renseigner votre nom'));
    die();
}

$name = $_POST['name'];

if(!isset($_POST['email']) || !is_string($_POST['email']) || !strlen($_POST['email'])) {   
    echo json_encode(array("code" => "KO","msg" => 'Veuillez renseigner votre adresse mail'));
    die();
}

$email = $_POST['email'];

if(!empty($_POST['tel'])) {   
    $phone = $_POST['tel'];
}

if(!isset($_POST['message']) || !is_string($_POST['message']) || !strlen($_POST['message'])) {   
    echo json_encode(array("code" => "KO","msg" => 'Veuillez renseigner le contenu de votre message'));
    die();
}
$message = $_POST['message'];


// Section envoie Mail

$myemail = 'contact@rabazatek.mg';
$mypwd = 'Contact@Rabazatek123//';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    //$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
    $mail->Host = 'rs7.websitehostserver.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $myemail;                 // SMTP username
    $mail->Password = $mypwd;                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->SMTPSecure = 'ssl';                            
    $mail->Port = 465;                                    // TCP port to connect to                              
    $mail->CharSet = 'UTF-8';

    //Recipients
    $mail->setFrom($myemail, 'Contact RABAZATEK');
    //$mail->addAddress('tommy12860@yahoo.fr', 'TOMMY Laha');     // Add a recipient
    $mail->addAddress('tommy12860@yahoo.fr');     // Add a recipient
    // $mail->addAddress('ellen@example.com');               // Name is optional
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('tommylaha@gmail.com');
    $mail->addBCC($myemail, 'Contact RABAZATEK');
    

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Test Envoie Mail Site Web Lewe';
    $body = '<b>Bonjour,</b><br/>';
    $body .= 'Une personne vient de nous contacter à partir du site web.<br/>';
    $body .= 'Ci-dessous les détails :<br/>';
    $body .= '<b>Son nom</b> : ' .$name. '<br/>';
    $body .= '<b>Son adresse mail</b> : ' .$email. '<br/>';
    if(strlen($phone)){
        $body .= '<b>Son numéro de téléphone</b> : ' .$phone. '<br/>';
    }
    $body .= '<b>Son message</b> : <blockquote>' .$message. '</blockquote><br/>';

    $mail->Body    = $body;
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo json_encode(array("code" => "OK","msg" => "Message envoyé"));

} catch (Exception $e) {
    echo json_encode(array("code" => "KO","msg" => 'Mailer Error: '. $mail->ErrorInfo));
}
