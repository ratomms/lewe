<!DOCTYPE html>
<html lang="fr" prefix="og: http://ogp.me/ns#">

<head>
    <title>Centre Hospitalier de Référence de District Niveau II à Betroka</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site web du Centre Medical à Betroka" />
    <meta name="keywords" content="CHRD Betroka, Centre Hopitalier, Hopital à Betroka, Hopital, Sud, Hopital du Sud, Santé Toliara, Santé Tulear, Santé du Sud, Chururgie à Betroka, Rabazatek, Rabaza" />
    <meta name="author" content="Rabazatek by TOMMY Laha" />
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Centre Hopitalier de Référence de Ditrict Niveau II à Betroka" />
    <meta property="og:description" content="Un Centre Hopitalier au Sud de Madagascar, dans la region d'ANOSY, ex-Province Tuléar" />
    <meta property="og:image" content="images/og_bg.JPG" />
    <meta property="og:url" content="http://chrd.rabazatek.mg" />
    <meta property="og:site_name" content="CHRD Betroka" />

    <link rel="shortcut icon" href="images/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body data-spy="scroll" data-target="#ftco-navbar" data-offset="200">

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="./"><strong>C.H.R.D Betroka</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a href="#section-home" class="nav-link">Accueil</a></li>
                    <li class="nav-item"><a href="#section-services" class="nav-link">Services</a></li>
                    <li class="nav-item"><a href="#section-galeries" class="nav-link">Galeries photos</a></li>
                    <li class="nav-item"><a href="#section-contact" class="nav-link">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END nav -->

    <!-- <section class="ftco-cover ftco-slant" style="background-image: url(images/bg_1.jpg);" id="section-home"> -->
    <section class="ftco-cover ftco-slant" style="background-image: url(images/bg_4.JPG);" id="section-home">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center ftco-vh-100">
                <div class="col-md-10">
                    <h1 class="ftco-heading ftco-animate text-home"><strong>Centre Hospitalier de Référence de District(C.H.R.D) Niveau II à BETROKA</strong></h1>
                    <!-- <h2 class="h5 ftco-subheading mb-5 ftco-animate">A free template by <a href="#">Free-Template.co</a></h2> -->
                    <h2 class="h5 ftco-subheading mb-6 ftco-animate">
                        <p>
                            <strong> Dans la région d'Anosy, de l'ex-Province de Tulear au Sud de Madagascar, se trouve ce C.H.R.D</strong><br/>
                            <strong>Ce centre de très bon accueil est dirigé par de medecins dynamiques et extremmement competents</strong>
                        </p>

                    </h2>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section ftco-slant" id="section-services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center ftco-animate">
                    <h2 class="text-uppercase ftco-uppercase">Nos Services</h2>
                    <div class="row justify-content-center mb-5">
                        <div class="col-md-7">
                            <p class="lead">Nous vous fournissons beaucoup de services d'une qualité irreprochable.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END row -->
            <div class="row">
                <div class="col-lg-4 mb-5 ftco-animate">
                    <figure><img src="images/urgence.jpg" alt="Churirgie" class="img-fluid"></figure>
                    <div class="p-3">
                        <h3 class="h4">Urgence</h3>
                        <p class="mb-4">Nous avons une accueil pour le service d'urgence</p>
                        <ul class="list-unstyled ftco-list-check text-left">
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Soin d'urgence</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Triage des patients</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Accueil soin infirmier</span></li>
                        </ul>
                        <!-- <p class="mb-2 text-center"><a href="#" class="btn btn-primary btn-sm">Gallery</a></p> -->
                    </div>
                </div>

                <div class="col-lg-4 mb-5 ftco-animate">
                    <figure><img src="images/maternite.jpg" alt="Matérnité" class="img-fluid"></figure>
                    <div class="p-3">
                        <h3 class="h4">Maternite</h3>
                        <p class="mb-4">Nous avons aussi de service maternite</p>
                        <ul class="list-unstyled ftco-list-check text-left">
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Accouchement</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Contraception et planning familiale</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Pathologie de la grossesse</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Operation cesarieenne</span></li>
                        </ul>
                        <!-- <p class="mb-2 text-center"><a href="#" class="btn btn-primary btn-sm">Gallery</a></p> -->
                    </div>
                </div>

                <div class="col-lg-4 mb-5 ftco-animate">
                    <figure><img src="images/medecine.jpg" alt="Médecine" class="img-fluid"></figure>
                    <div class="p-3">
                        <h3 class="h4">Medecine</h3>
                        <p class="mb-4">Nous avons un service de la medecine générale</p>
                        <ul class="list-unstyled ftco-list-check text-left">
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Centre de diagnostic</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Traitement pathologie medicale et tropicale</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Reanimation</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Pharmacie</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Laboratoire</span></li>
                        </ul>
                        <!-- <p class="mb-2 text-center"><a href="#" class="btn btn-primary btn-sm">Gallery</a></p> -->
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-lg-4 mb-1 ftco-animate">
                    <figure><img src="images/churirgie.jpg" alt="Chirurgie" class="img-fluid"></figure>
                    <div class="p-3">
                        <h3 class="h4">Chirurgie</h3>
                        <p class="mb-4">Nous faisons beaucoup d'intervention chirurgicale à savoir :</p>
                        <ul class="list-unstyled ftco-list-check text-left">
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Pratique appendicectomie</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Cure hernie inguinale</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Kystectomie ovarienne</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Cure grossesse extrauterine</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Hysterectomie</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Resection anastomose</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Digestive</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Splenectomie</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Adenomectomie prostatique</span></li>
                        </ul>
                        <!-- <p class="mb-2 text-center"><a href="#" class="btn btn-primary btn-sm">Gallery</a></p> -->
                    </div>
                </div>

                <div class="col-lg-4 mb-1 ftco-animate">
                    <figure><img src="images/echographie.jpg" alt="Echographie" class="img-fluid"></figure>
                    <div class="p-3">
                        <h3 class="h4">Echographie</h3>
                        <p class="mb-4">Autre de ceux qui sont cités ci-dessus, nous avons aussi un service d'échographie</p>
                        <ul class="list-unstyled ftco-list-check text-left">
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Abdominale et pelvienne</span></li>
                            <li class="d-flex mb-2"><span class="oi oi-check mr-3 text-primary"></span> <span>Obstetrical</span></li>
                        </ul>
                        <!-- <p class="mb-2 text-center"><a href="#" class="btn btn-primary btn-sm">Gallery</a></p> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--<section class="ftco-section bg-light  ftco-slant ftco-slant-white" id="section-features">-->
    <section class="ftco-section bg-light  ftco-slant ftco-slant-white" id="section-galeries">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center mb-5 ftco-animate">
                    <h2 class="text-uppercase ftco-uppercase">Photos</h2>
                    <div class="row justify-content-center">
                        <div class="col-md-7">
                            <p class="lead">Galeries photos du centre C.H.R.D Betroka dans un paysage accueillant.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="owl-carousel ftco-owl">

                        <!-- Block 1 de la galerie qui s'affiche du Lundi et Jeudi -->
                        <?php 
                            $i = date('w'); 
                            $lu_je = array(1, 4);
                            $ma_ven = array(2, 5);
                            $me_sa = array(3, 6);


                            if(in_array($i,$lu_je)){

                        ?>
                        <div class="item ftco-animate">
                            <img src="images/photo_01.JPG" alt="Los Angeles" style="width:100%;">
                        </div>

                        <div class="item ftco-animate">
                            <img src="images/photo_02.JPG" alt="New york" style="width:100%;">
                        </div>

                        <div class="item ftco-animate">
                            <img src="images/photo_03.JPG" alt="Chicago" style="width:100%;">
                        </div>

                        <div class="item ftco-animate">
                            <img src="images/photo_04.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_05.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_06.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_07.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_08.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_09.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <!-- End Block 1 -->
                        <?php 
                            
                            } else if(in_array($i,$ma_ven)){

                        ?>

                        <!-- Block 2 de la galerie qui s'affiche du Mardi et Vendredi -->
                        <div class="item ftco-animate">
                            <img src="images/photo_10.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_11.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_12.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_13.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_14.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_15.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_16.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_17.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_18.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_19.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_20.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <?php 
                            
                            } else if(in_array($i,$me_sa)) { 
                        
                        ?>
                        <!-- End Block 2 -->

                        <!-- Block 3 de la galerie qui s'affiche du Mercredi, Samedi et Dimanche -->
                         <div class="item ftco-animate">
                            <img src="images/photo_21.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_22.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_23.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_24.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_25.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_26.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_27.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_28.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_29.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_30.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_31.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_32.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_33.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_34.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_35.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <?php 

                            } else {
                        

                        ?>
                        <div class="item ftco-animate">
                            <img src="images/photo_36.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_37.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <div class="item ftco-animate">
                            <img src="images/photo_38.JPG" alt="Los Angeles" style="width:100%;">
                        </div>
                        <?php

                            }

                        ?>
                        <!-- End Block 3 -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->

    <section class="ftco-section bg-white ftco-slant ftco-slant-dark" id="section-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center ftco-animate mb-5">
                    <h2 class="text-uppercase ftco-uppercase">Nous Contacter</h2>
                    <div class="row justify-content-center">
                        <div class="col-md-7">
                            <p class="lead">Nous sommes ouvert à tout partenariat entre un particulier ou un centre hospitalier et nous pouvons recevoir des stagiares(medecin, sage femmme, infirmier ou chirurgien) qui veulent perfectionner ses competences.
                                Pour avoir beaucoup plus de détails, n'hesitez pas de nous contacter en envoyant de message ici-bas ou par téléphone.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md pr-md-5 mb-5">
                    <h2>Votre message</h2>
                    <div id="message-sent-alert" style="display:none;" class="alert alert-success alert-dismissible fade show" role="alert">
                        Votre message a été envoyé.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="message-error-alert" style="display:none;" class="alert alert-warning alert-dismissible fade show" role="alert">
                        Erreur d'envoie de votre message.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form name="form-contact" id="form-contact" action="./contact.php" method="post">
                        <div class="form-group">
                            <label for="name" class="sr-only">Nom</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Saisissez votre nom" required>
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Saisissez votre adresse e-mail" required>
                        </div>
                        <div class="form-group">
                            <label for="tel" class="sr-only">Téléphone</label>
                            <input type="tel" class="form-control" id="tel" name="tel" placeholder="Saisissez votre numéro de téléphone">
                        </div>
                        <div class="form-group">
                            <label for="message" class="sr-only">Message</label>
                            <textarea name="message" id="message" cols="30" rows="8" class="form-control" placeholder="Ecrivez votre message" required></textarea>
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" class="btn btn-primary" value="Envoyer">
                        </div>
                    </form>
                </div>
                <div class="col-md" id="map">
                </div>
            </div>
        </div>
    </section>
    <footer class="ftco-footer ftco-bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ftco-footer-widget mb-4">
                        <strong>Dr. Hary Antenaina MIHASON, Chef de Centre</strong><br/>
                        <strong>Téléphone</strong> : 034 79 988 55<br/>
                        <strong>E-mail</strong> : mihasonharyantenaina@yahoo.fr
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ftco-footer-widget mb-4">
                        <ul class="ftco-footer-social list-unstyled float-md-right float-lft">
                            <!--  <li><a href="#"><span class="icon-twitter"></span></a></li>-->
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <!--  <li><a href="#"><span class="icon-instagram"></span></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md text-left">
                    <p>&copy; CHRD Betroka 2018. All Rights Reserved. Designed by <a href="http://www.rabazatek.mg/">RABAZATEK</a></p>
                </div>
            </div>
        </div>
    </footer>

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#4586ff"/>
        </svg>
    </div>
    <!-- Mon loader implementation  -->
    <div id="rbt-loader" class="hide mediumscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#4586ff"/>
        </svg>
        <p class="text-loader">
            <strong>Envoie encours, veuillez patienter</strong>
        </p>
    </div>


    <!--     <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> -->

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9l7DaOw09_B4gwkbN9N-KKf8gArWNIMM&callback=initMap"></script>
    <script src="js/google-map.js"></script>

    <script src="js/main.js"></script>
</body>

</html>