function initMap() {
    //var Manambondro = { lat: -23.770831, lng: 47.551819 };
    var Betroka = { lat: -23.2678801, lng: 46.0808871 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: Betroka
    });

    var marker = new google.maps.Marker({
        position: Betroka,
        map: map
            //icon: 'images/loc.png' 
    });
}