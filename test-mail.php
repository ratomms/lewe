<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';


// Section envoie Mail

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    //$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
    $mail->Host = 'rs7.websitehostserver.net';  // Specify main and backup SMTP servers
    //$mail->Host = 'mail.rabazatek.mg';  
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'contact@rabazatek.mg';                 // SMTP username
    $mail->Password = 'secret';                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->SMTPSecure = 'ssl';                            
    $mail->Port = 465;                                    // TCP port to connect to
    //$mail->Port = 25;                                   

    //Recipients
    $mail->setFrom('contact@rabazatek.mg', 'Contact RABAZATEK');
    //$mail->addAddress('tommy12860@yahoo.fr', 'TOMMY Laha');     // Add a recipient
    $mail->addAddress('tommy12860@yahoo.fr');     // Add a recipient
    // $mail->addAddress('ellen@example.com');               // Name is optional
    //$mail->addReplyTo('contact@rabazatek.mg', 'Contact RABAZATEK');
    $mail->addCC('tommylaha@gmail.com');
    $mail->addBCC('contact@rabazatek.mg', 'Contact RABAZATEK');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Changer la langue
    //$mail->setLanguage('fr', 'vendor/phpmailer/phpmailer/language/');

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Test Envoie Mail Site Web Lewe';
    $body = '<b>Bonjour,</b><br/>';
    $body .= 'Une personne vient de nous contacter à partir du site web.<br/>';
    $body .= 'Ci-dessous les détails :<br/>';
    $body .= '<b>Son nom</b> : TOMMY <br/>';
    $body .= '<b>Son adresse mail</b> : test@mail.com<br/>';
    $body .= '<b>Son message</b> : <blockquote>Ceci est un test</blockquote><br/>';

    //$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->Body    = $body;
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo json_encode(array("status" => "OK","msg" => "Message envoyé"));

} catch (Exception $e) {
    //echo 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
    echo json_encode(array("status" => "KO","msg" => 'Mailer Error: '. $mail->ErrorInfo));
}